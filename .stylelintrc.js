module.exports = {
  extends: ["stylelint-config-standard-scss", "stylelint-config-prettier-scss"],
  ignoreFiles: [
    "**/*/*.js",
    "**/*/*.html",
    "src/**/*.css",
    "src/**/*.map",
    "src/assets/**/*",
  ],
  rules: {
    "font-family-name-quotes": "always-unless-keyword",
    "selector-class-pattern":
      "^([a-z][a-z0-9]*)(-[a-z0-9]+)*((__([a-z][a-z0-9]*)(-[a-z0-9]+)*)?(--([a-z][a-z0-9]*)(-[a-z0-9]+)*)?)$",
    "selector-id-pattern":
      "^([a-z][a-z0-9]*)(-[a-z0-9]+)*((__([a-z][a-z0-9]*)(-[a-z0-9]+)*)?(--([a-z][a-z0-9]*)(-[a-z0-9]+)*)?)$",
  },
};
