const rootEl = document.querySelector(":root");
const htmlEl = document.documentElement;
const formEl = document.querySelector(".form");
const emailInput = document.getElementById("form__control--email");
const phoneInput = document.getElementById("form__control--phone");
const nameInput = document.getElementById("form__control--name");
const addressTextarea = document.getElementById("form__control--address");
const cityInput = document.getElementById("form__control--city");
const countrySelect = document.getElementById("form__control--country");
const zipInput = document.getElementById("form__control--postal-code");
const resultsEl = document.querySelector(".results");

const rootStyles = getComputedStyle(rootEl);
const largeScreenReferenceSize =
  +rootStyles.getPropertyValue("--big-screen-reference-size").slice(0, -2) * 16;
const smallScreenReferenceSize =
  +rootStyles.getPropertyValue("--small-screen-reference-size").slice(0, -2) *
  16;
const smallScreenThresholdProp = rootStyles.getPropertyValue(
  "--small-screen-threshold",
);
const smallScreenThreshold = +smallScreenThresholdProp.slice(0, -2) * 16;

/* Make textarea auto-scrollable to the bottom */
addressTextarea.onkeyup = () => {
  addressTextarea.scrollTop = addressTextarea.scrollHeight;
};

function getAriaDescriptionEl(el) {
  const ariaDescId = el.getAttribute("aria-describedby");
  return document.getElementById(ariaDescId);
}

const validatingFormControls = new Map([
  [
    emailInput,
    {
      validationMessage:
        "Please enter valid email address. It should contain “@” character.",
      nextControlOnEnter: phoneInput,
    },
  ],
  [
    phoneInput,
    {
      validationMessage: "Please enter your phone number.",
      nextControlOnEnter: nameInput,
    },
  ],
  [
    nameInput,
    {
      validationMessage: "Please enter your full name.",
      nextControlOnEnter: addressTextarea,
    },
  ],
  [
    addressTextarea,
    {
      validationMessage: "Please enter your address.",
      nextControlOnShiftEnter: cityInput,
    },
  ],
  [
    cityInput,
    {
      validationMessage: "Please enter your city.",
      nextControlOnEnter: countrySelect,
    },
  ],
  [
    countrySelect,
    {
      validationMessage: "Please select your country.",
      nextControlOnEnter: zipInput,
    },
  ],
]);

/* Add control validating and switching */
function switchToNextControl(control) {
  control.focus();
  const controlTag = control.tagName.toLowerCase();
  const controlType = control.type.toLowerCase();
  if (
    (controlTag === "input" || controlTag === "tag") &&
    controlType !== "checkbox"
  ) {
    control.selectionStart = control.selectionEnd = control.value.length;
  }
}

function showErrorMessage(control, message) {
  const controlParent = control.parentElement;
  const controlAriaDesc = getAriaDescriptionEl(control);
  const isValid = control.validity.valid;
  if (isValid) {
    controlAriaDesc.classList.add("sr-only");
    controlAriaDesc.innerText = "";
    if (control === countrySelect) {
      controlParent.classList.remove("grid-row-original", "grid-row-full");
      zipInput.parentElement.classList.remove(
        "grid-row-original",
        "grid-row-full",
      );
    }
  } else {
    controlAriaDesc.classList.remove("sr-only");
    controlAriaDesc.innerText = message;
    if (control === countrySelect) {
      controlParent.classList.add("grid-row-original", "grid-row-full");
      zipInput.parentElement.classList.add(
        "grid-row-original",
        "grid-row-full",
      );
    }
  }
}

function reactToEnterOnControl(event, nextControlProp) {
  const control = event.target;

  if (!validatingFormControls.has(control)) {
    return;
  }

  const controlProps = validatingFormControls.get(control);
  const validationMsg = controlProps.validationMessage;
  const nextControl = controlProps[nextControlProp];
  const isValidControl = control.validity.valid;

  event.preventDefault();

  if (isValidControl) {
    switchToNextControl(nextControl);
  } else {
    showErrorMessage(control, validationMsg);
  }
}

for (const [control, props] of validatingFormControls.entries()) {
  const { validationMessage } = props;
  control.oninvalid = () => {
    control.setCustomValidity(validationMessage);
  };
  control.onchange = () => {
    control.setCustomValidity("");
  };
  control.addEventListener("focusout", (e) => {
    e.preventDefault();
    showErrorMessage(control, validationMessage);
  });
  control.addEventListener("keydown", (e) => {
    if (e.key !== "Enter") {
      return;
    }

    if ("nextControlOnEnter" in props) {
      reactToEnterOnControl(e, "nextControlOnEnter");
    } else if ("nextControlOnShiftEnter" in props && e.shiftKey) {
      reactToEnterOnControl(e, "nextControlOnShiftEnter");
    }
  });
}

/* Show successful order message on submission */
formEl.addEventListener("submit", (event) => {
  event.preventDefault();
  resultsEl.style.visibility = "visible";
  resultsEl.classList.remove("results--hidden");
  resultsEl.classList.add("results--shown");
});

/* Accommodate font on window resize */
function getFontChangeRate(currentSize, referenceSize) {
  return currentSize <= referenceSize ? 0.75 : 1;
}

function onResize() {
  const currentSize = window.innerWidth;
  const referenceSize =
    currentSize <= smallScreenThreshold
      ? smallScreenReferenceSize
      : largeScreenReferenceSize;
  const fontChangeRate = getFontChangeRate(currentSize, referenceSize);
  const baseFontMultiplier = Math.pow(
    currentSize / referenceSize,
    fontChangeRate,
  );
  const newBaseFontRatio = baseFontMultiplier * 62.5;
  htmlEl.style.fontSize = `${newBaseFontRatio.toFixed(4)}%`;
}

window.addEventListener("resize", onResize);
onResize();
