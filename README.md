<h1 align="center">Checkout Page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/checkout-page">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/checkout-page">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/0J1NxxGhOUYVqihwegfO">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

Simple checkout form built with semantic HTML, CSS, a little bit of vanilla JS and accessibility in mind. CSS and JS used are reasonably modern, i.e. without regard to backwards compatibility.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)
- JS (Vanilla Flavor)

## Features

Accessibility includes semantic markup, aria-live regions and stylistic choices for forms.

JS is used to make:

1. font responsive;
2. display error messages when user focuses out of input;
3. show success message on form submission.

## Acknowledgements

- [A modern CSS Reset](https://piccalil.li/blog/a-modern-css-reset/)
- [Icons by Google](https://google.github.io/material-design-icons/)
- [How to Favicon by Andrey Sitnik](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs)
- [Pure CSS Custom Checkbox Style by Stephanie Eckles](https://moderncss.dev/pure-css-custom-checkbox-style/)
- [Custom Select Styles with Pure CSS by Stephanie Eckles](https://moderncss.dev/custom-select-styles-with-pure-css/)
- [How to Show a Placeholder for a <select> Tag](https://markus.oberlehner.net/blog/faking-a-placeholder-in-a-html-select-form-field/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
